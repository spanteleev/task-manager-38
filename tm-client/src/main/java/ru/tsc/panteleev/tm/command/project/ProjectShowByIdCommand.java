package ru.tsc.panteleev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.project.ProjectGetByIdRequest;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display project by id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        Project project = getProjectEndpoint().showProjectById(new ProjectGetByIdRequest(getToken(), id)).getProject();
        showProject(project);
    }

}
