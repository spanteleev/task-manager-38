package ru.tsc.panteleev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.AbstractModel;
import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @Nullable
    M add(@NotNull M model);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @Nullable
    M findById(@NotNull String id);

    @Nullable
    M remove(@NotNull M model);

    @Nullable
    M removeById(@NotNull String id);

    void clear();

    boolean existsById(@NotNull String id);

    long getSize();

}
