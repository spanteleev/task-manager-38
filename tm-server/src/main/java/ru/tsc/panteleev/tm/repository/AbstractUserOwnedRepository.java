package ru.tsc.panteleev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final M model);

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> models = new Vector<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE user_id = ? ", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next())
                models.add(fetch(rowSet));
            return models;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null) return Collections.emptyList();
        @NotNull final List<M> models = new Vector<>();
        @NotNull String sql = String.format("SELECT * FROM %s WHERE user_id = ? ", getTableName());
        if (sort != null)
            sql += "ORDER BY " + sort.getOrderColumn();
        System.out.println(sql);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next())
                models.add(fetch(rowSet));
            return models;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE row_id = ? AND user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        return removeById(userId, model.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findById(userId, id);
        @NotNull final String sql = String.format("DELETE FROM %s WHERE row_id = ? AND user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s WHERE user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            return statement.executeQuery().getRow();
        }
    }
}
