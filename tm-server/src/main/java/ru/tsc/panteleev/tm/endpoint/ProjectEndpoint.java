package ru.tsc.panteleev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.panteleev.tm.api.service.IProjectService;
import ru.tsc.panteleev.tm.api.service.IServiceLocator;
import ru.tsc.panteleev.tm.dto.request.project.*;
import ru.tsc.panteleev.tm.dto.response.project.*;
import ru.tsc.panteleev.tm.enumerated.Sort;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.panteleev.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Date dateBegin = request.getDateBegin();
        @Nullable final Date dateEnd = request.getDateEnd();
        @Nullable Project project = getProjectService().create(userId, name, description, dateBegin, dateEnd);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable Project project = getProjectService().changeStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCompleteByIdResponse completeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = getProjectService().changeStatusById(userId, id, Status.COMPLETED);
        return new ProjectCompleteByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = getProjectService().removeById(userId, id);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = getProjectService().findById(userId, id);
        return new ProjectGetByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable Project project = getProjectService().changeStatusById(userId, id, Status.IN_PROGRESS);
        return new ProjectStartByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

}
