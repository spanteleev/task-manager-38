package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.repository.IUserOwnedRepository;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.AbstractUserOwnedModel;
import ru.tsc.panteleev.tm.model.Project;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    M changeStatusById(String userId, String id, Status status);

}
