package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IUserOwnedService<Project> {

    @NotNull
    Project create(@Nullable String userId,
                   @Nullable String name,
                   @Nullable String description,
                   @Nullable Date dateBegin,
                   @Nullable Date dateEnd
    );

    Project updateById(String userId, String id, String name, String description);

}
