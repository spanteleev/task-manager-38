package ru.tsc.panteleev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IRepository;
import ru.tsc.panteleev.tm.model.AbstractModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    protected abstract M fetch(@NotNull final ResultSet rowSet);

    protected Timestamp convertDateToTimestamp(@Nullable final Date date) {
        if (date == null) return null;
        return new Timestamp(date.getTime());
    }

    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        @NotNull List<M> result = new ArrayList<>();
        models
                .stream()
                .forEach(model -> result.add(add(model)));
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> models = new Vector<>();
        @NotNull final String sql = String.format("SELECT * FROM %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next())
                models.add(fetch(rowSet));
            return models;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@NotNull final String id) {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE row_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE row_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findById(id);
        return model == null ? null : remove(model);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("TRUNCATE TABLE %s ", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findById(id) != null;
    }

    @Override
    @SneakyThrows
    public long getSize() {
        @NotNull final String sql = String.format("SELECT COUNT(1) FROM %s ", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            return statement.executeQuery().getRow();
        }
    }

}
