package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.endpoint.*;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.SystemUtil;
import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    public void registry(Object endpoint) {
        final String host = getPropertyService().getServerHost();
        final String port = getPropertyService().getServerPort().toString();
        final String name = endpoint.getClass().getSimpleName();
        final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        backup.start();
        initUser();
        registryShutdownHookOperation();
        loggerService.info("** TASK-MANAGER SERVER IS STARTING **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
                backup.stop();
            }
        });
    }

    @SneakyThrows
    private void initUser() {
        @Nullable final User admin = userService.findByLogin("admin");
        if (admin == null)
            userService.create("admin", "admin", Role.ADMIN);
        @Nullable final User test = userService.findByLogin("test");
        if (test == null)
            userService.create("test", "test", Role.USUAL);
    }

    public void run() {
        runStartupOperation();
    }

}
