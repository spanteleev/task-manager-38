package ru.tsc.panteleev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String TABLE_NAME = "\"TM_PROJECT\"";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Project fetch(@NotNull final ResultSet rowSet) {
        @NotNull final Project project = new Project();
        project.setId(rowSet.getString("row_id"));
        project.setName(rowSet.getString("name"));
        project.setDescription(rowSet.getString("description"));
        project.setStatus(Status.toStatus(rowSet.getString("status")));
        project.setCreated(rowSet.getTimestamp("created_dt"));
        project.setDateBegin(rowSet.getTimestamp("begin_dt"));
        project.setDateEnd(rowSet.getTimestamp("end_dt"));
        project.setUserId(rowSet.getString("user_id"));
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project add(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final String sql = String.format(
                "INSERT INTO %s (row_id, name, description, status, created_dt, begin_dt, end_dt, user_id) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setString(3, project.getDescription());
            statement.setString(4, project.getStatus().toString());
            statement.setTimestamp(5, convertDateToTimestamp(project.getCreated()));
            statement.setTimestamp(6, convertDateToTimestamp(project.getDateBegin()));
            statement.setTimestamp(7, convertDateToTimestamp(project.getDateEnd()));
            statement.setString(8, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET name = ?, description = ?, status = ? WHERE row_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
