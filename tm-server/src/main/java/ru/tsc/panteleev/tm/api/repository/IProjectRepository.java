package ru.tsc.panteleev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    Project update(@NotNull Project project);

}
